from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from observer.serializers import WorkerSerializer, CoordinatesSerializer, WorkerLatest
from observer.models import Worker, Coordinates
from rest_framework.views import APIView
from rest_framework.response import Response
from datetime import datetime
from datetime import timedelta
from observer.generator_c import moving
from rest_framework.exceptions import ValidationError


# Для удобства просмотра изменений
class WorkerViewSet(viewsets.ModelViewSet):
    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer


# Для удобства просмотра изменений
class CoordinatesViewSet(viewsets.ModelViewSet):
    queryset = Coordinates.objects.all()
    serializer_class = CoordinatesSerializer


class WorkerAdd(APIView):
    def get(self, request):
        print(request.query_params)
        w = WorkerSerializer(data=request.query_params)
        print(w.is_valid())
        print('w: ', w)
        print('w.data: ', w.validated_data)
        if w.is_valid():
            w.save()
            return Response(['OK'])
        else:
            raise ValidationError(w.errors)

    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer



class IdToWorker(APIView):
    def get(self, request):
        r = int(request.query_params['id'])
        if Worker.objects.filter(id=r).exists(): #, ' - вот оно, есть ли объект')
            obj = Worker.objects.get(pk=r)
            print(obj, type(obj))
            return Response(WorkerSerializer(obj).data)
        else:
            raise ValidationError(detail='id не существует')

    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer


class DelWorker(APIView):
    def get(self, request):
        r = int(request.query_params['id'])
        z = Worker.objects.filter(id=r)
        if len(z) != 0:
            z.delete()
            return Response(['OK'])
        else:
            raise ValidationError(detail='id не существует')

    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer


class EditWorker(APIView):
    def get(self, request):
        q = request.query_params
        try:
            id = int(request.query_params['id'])
        except:
            raise ValidationError(detail='id не передан')
        try:
            worker = Worker.objects.get(pk=id)
        except:
            raise worker.DoesNotExist
        for key in q:            # в следующий раз разберусь с serializer update, что-то пошло не так
            print(key, type(key))
            if key == 'id':
                continue
            if key == 'name'and q.get('name', None):
                worker.name = q['name']
            if key == 'age'and q.get('age', None):
                worker.age = q['age']
            if key == 'sex'and q.get('sex', None):
                worker.sex = q['sex']
        try:
            worker.save()
        except:
            raise ValidationError(detail='некорректные данные')
        return Response(['OK'])

    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer


class GenCoord(APIView):
    def get(self, request):
        print('Смотрим генератор координат:')
        r = request.query_params
        try:
            id = int(r['id'])
            ltln = r['ltln'].split(',')
            lt = float(ltln[0])
            ln = float(ltln[1])
            time = r['time'].split(',')
            t1 = datetime.strptime(time[0], "%Y.%m.%d %H:%M:%S")
            t2 = datetime.strptime(time[1], "%Y.%m.%d %H:%M:%S")  # class datetime.datetime
        except:
            raise ValidationError(detail='Данные некорректны')
        # разобрались что где: id - int, lt ln - float, t1 t2 - datetime.datetime
        # приступаем к генерации. Цикл будет по минутам, а генерация координат тестовых точек будет
        # в отдельном модуле
        if t1 > t2:
            t1, t2 = t2, t1  # На случай попытки повернуть время вспять
        td = t2 - t1
        print('td: ', td, type(td))
        minute = td.seconds//60
        print("minute", minute)
        for i in range(1, minute+1):
            ti = timedelta(0, i*60) + t1  # ti - генерируемое время для текущей координаты
            lt, ln = moving(lt, ln)
            Coordinates.objects.create(time=ti, latitude=lt, longitude=ln, user=Worker.objects.get(pk=id))
        return Response(['OK'])

    queryset = Coordinates.objects.all()
    serializer_class = CoordinatesSerializer


class GetHist(APIView):
    def get(self, request):
        r = request.query_params
        try:
            id = int(r['id'])
            time = r['time'].split(',')
            t1 = datetime.strptime(time[0], "%Y.%m.%d %H:%M:%S")
            t2 = datetime.strptime(time[1], "%Y.%m.%d %H:%M:%S")  # class datetime.datetime
        except:
            return Response(['NOT OK'])
        if t1 > t2:
            t1, t2 = t2, t1  # На случай попытки повернуть время вспять
        print()
        print("id: ", id, t1, t2)
        print()
        # q = Coordinates.objects.filter(user=Worker.objects.get(id=id), time__range=(t1, t2))
        queryset = Coordinates.objects.filter(user=Worker.objects.get(pk=id)).filter(time__gte=t1, time__lte=t2)
        geodata = CoordinatesSerializer(queryset, many=True)
        return Response(geodata.data)

    queryset = Coordinates.objects.all()
    serializer_class = CoordinatesSerializer


class LatestGeo(viewsets.ModelViewSet):
    queryset = Worker.objects.all()
    serializer_class = WorkerLatest

