from django.db import models

# Create your models here.

from django.conf import settings
from django.db import models
from django.utils import timezone
import datetime


class Worker(models.Model):
    name = models.CharField(max_length=200, blank=False)
    sex = models.CharField(max_length=6, blank=False)
    age = models.IntegerField()


class Coordinates(models.Model):
    user = models.ForeignKey(Worker, related_name='coordinates', on_delete=models.CASCADE)
    time = models.DateTimeField(blank=False)
    latitude = models.FloatField(max_length=9, blank=False)
    longitude = models.FloatField(max_length=9, blank=False)

