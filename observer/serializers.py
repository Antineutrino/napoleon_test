from django.contrib.auth.models import User, Group
from rest_framework import serializers
from observer.models import Worker, Coordinates


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')



class CoordinatesSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.SerializerMethodField()
    time = serializers.DateTimeField()
    latitude = serializers.FloatField()
    longitude = serializers.FloatField()

    def get_user(self, obj):
        return obj.user.id

    class Meta:
        model = Coordinates
        fields = ('user', 'time', 'longitude', 'latitude')

    def create(self, validated_data):
        print('Сохранение координаты, validated_data', validated_data)
        return Coordinates.objects.create(**validated_data)


class WorkerSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    sex = serializers.CharField(max_length=6)
    age = serializers.IntegerField()
    id = serializers.ReadOnlyField()


    class Meta:
        model = Worker
        print('Сработал WorkerSerializer класс Meta')
        fields = ('name', 'sex', 'age', 'id')

    def create(self, validated_data):
        print('validated_data', validated_data)
        return Worker.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.sex = validated_data.get('sex', instance.sex)
        instance.age = validated_data.get('age', instance.age)
        instance.save()
        print('validated_data update', validated_data)
        return instance



class WorkerLatest(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    # sex = serializers.CharField(max_length=6)
    # age = serializers.IntegerField()
    # id = serializers.ReadOnlyField()
    coordinates = serializers.SerializerMethodField()

    class Meta:
        model = Worker
        # print('Сработал WorkerSerializer класс Meta')
        fields = ('name', 'id')
    def get_coordinates(self, worker):
        print('Сериализатор последних координат')
        try:
            result = CoordinatesSerializer(Coordinates.objects.filter(user=worker).latest('time')).data
        except:
            result = []
        return result