# Информация с википедии о длинах окружностей и эллипсов
# Окружность большого круга	40 075,017 км (по экватору)
# 40 007,86 км (по меридиану)

from math import sin, cos, radians, degrees, pi
from random import uniform


def moving(lt, ln):
    Se = 40075.017 * 1000               # периметр по экватору в метрах
    Sp = Se * cos(radians(ln))          # периметр по параллели в метрах
    Sm = 40007.86 * 1000                # периметр по мередиану в метрах

    S = uniform(0, 100)                 # генерируем расстояние
    alpha = uniform(0, 2 * pi)          # генерируем угол в радианах. Почти как азимут, но не азимут.
    Xs = S * cos(alpha)                 # смещениие в метрах по долготе
    Ys = S * sin(alpha)                 # смещение в метрах по широте

    lam = degrees((Xs / Sp) * 2 * pi)   # пересчитываем смещение в градусы
    phi = degrees((Ys / Sm) * 2 * pi)   # пересчитываем смещение в градусы

    lt_n = round(lt + lam, 6)
    ln_n = round(ln + phi, 6)


    # print('S: ', S)                   # отладочный блок
    # print('alpha: ', alpha)
    # print(Xs, Ys)
    # print("lam: ", lam)
    # print("phi: ", phi)

    return lt_n, ln_n





