# Napoleon_test


Добавление сотрудника:
/addworker/?name='name'&sex='male'&age='age'
Выдаётся ОК в случае если параметры переданы подходящие, и NOT OK если что-то не так

Удаление сотрудника (производится по id записи в БД):
/delworker/?id='id'
Выдаётся ОК в случае если такая запись была, и NOT OK если такой записи не было

Получение данных о сотруднике:
/idtoworker/?id='4'
Выдаются данные в случае если такая запись есть, и NOT OK если такой записи нет

Обновление данных о сотруднике (смена имени, взросление, смена пола):
/editworker/?id='id'&age='age'&name='name'&sex='sex'
Принимает произвольный набор параметров, входящих в параметры сотрудника, если какой-то параметр включится
два раза - сохранится последнее значение

Генерация тестовых данных, в качестве параметров передаются id сотрудника, начальные координаты (широта и долгота),
и время начала и конца временного промежутка генерации координат
/gencoord/?id='id'&ltln=xx.yyyyyy,xx.yyyyyy&time=YYYY.MM.DD hh:mm:ss,YYYY.MM.DD hh:mm:ss

Получение истории перемещений для сотрудника в заданный промежуток времени
/gethist/?id='id'&time=YYYY.MM.DD hh:mm:ss,YYYY.MM.DD hh:mm:ss

Получение данных о последнем местоположении всех сотрудников.
/latestgeo/