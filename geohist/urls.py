"""geohist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from observer import views

router = routers.DefaultRouter()
router.register(r'workers', views.WorkerViewSet)
router.register(r'latestgeo', views.LatestGeo, basename='')
router.register(r'coordinates', views.CoordinatesViewSet)





urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path(r'addworker/', views.WorkerAdd.as_view(), name='addworker'),
    path(r'idtoworker/', views.IdToWorker.as_view(), name='IdToWorker'),
    path(r'delworker/', views.DelWorker.as_view(), name='DelWorker'),
    path(r'editworker/', views.EditWorker.as_view(), name='EditWorker'),
    path(r'gencoord/', views.GenCoord.as_view(), name='GenCoord'),
    path(r'gethist/', views.GetHist.as_view(), name='GetHist'),

    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
